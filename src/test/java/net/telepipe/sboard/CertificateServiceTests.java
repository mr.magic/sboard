package net.telepipe.sboard;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import net.telepipe.sboard.model.Certificate;
import net.telepipe.sboard.service.CertificateService;

import static org.assertj.core.api.Assertions.*;


@SpringBootTest
public class CertificateServiceTests {
    public static final String crt =
        "-----BEGIN CERTIFICATE-----\n"+
        "MIICpjCCAY4CCQCQV4ALpycgIDANBgkqhkiG9w0BAQsFADAVMRMwEQYJKoZIhvcN\n"+
        "AQkBFgR0ZXN0MB4XDTIwMDQwOTEzMzUxOFoXDTIxMDQwOTEzMzUxOFowFTETMBEG\n"+
        "CSqGSIb3DQEJARYEdGVzdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB\n"+
        "AOYytZak6DckmuSco4/M9iq2nwt+TJLA+qVqtNCFccBMW/AQO6hQ7P7u0sOZbTqr\n"+
        "+Tn7Js3qWOvFb0BDe2V/5upyPgraKWOHfucylD/gI38eoNiBIDPFRJh3rkWRk2tc\n"+
        "DQ42iknYDR2e1oOJJqBZ3lTTnUNH5Z7jnMFKVe9d6kguyTyF5fX2NYGAHRKUX/nS\n"+
        "3cUhzGVt7wER/F8owT7CB7eNZ41IC27IFI+uIVp774VGcrdcbg/UoCd4Gv01tfwF\n"+
        "RC8Pl2rTpy0KNEFG59V8eZOZSTZhO4QgeJU/eiQDdE5xNkLK0Exg8R7YGsKw1g09\n"+
        "Sf0qw11Ahn7wX1uni3AXd3kCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEASC5Q31Ir\n"+
        "x0j+euT624P+dKCLGMfg2vfdga2DIIO2QATDX1yetlIMKEtKL/uoWyZYZ76xPaNj\n"+
        "tVYdmE+qr+lhoUkOgT08UVJD8q0Bex2oP41/nRgiJUWlVxl/cJlhCyld42OmhyN4\n"+
        "TZ1wfr+3GYIZ7BbuC9O+c+pUY374KNF+MeWLdb/Ovzg/b0eTsQUYBUg/n+RiqOaM\n"+
        "ZCxvtF4TzBs0xaNLbMMLQD9QLLJb78kGm9RQV2Juf6ugNDwKEmuRwZnBOYpDDfgk\n"+
        "iNxGuWWi3XqVGm3a6wdCVVTn5ASDxLZlftgr2pacF7+4buqkmlW7ij5FU9a0jEdT\n"+
        "EB5HC6b13cWlUg==\n"+
        "-----END CERTIFICATE-----\n";

    public static final String key = 
        "-----BEGIN PRIVATE KEY-----\n"+
        "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDmMrWWpOg3JJrk\n"+
        "nKOPzPYqtp8LfkySwPqlarTQhXHATFvwEDuoUOz+7tLDmW06q/k5+ybN6ljrxW9A\n"+
        "Q3tlf+bqcj4K2iljh37nMpQ/4CN/HqDYgSAzxUSYd65FkZNrXA0ONopJ2A0dntaD\n"+
        "iSagWd5U051DR+We45zBSlXvXepILsk8heX19jWBgB0SlF/50t3FIcxlbe8BEfxf\n"+
        "KME+wge3jWeNSAtuyBSPriFae++FRnK3XG4P1KAneBr9NbX8BUQvD5dq06ctCjRB\n"+
        "RufVfHmTmUk2YTuEIHiVP3okA3ROcTZCytBMYPEe2BrCsNYNPUn9KsNdQIZ+8F9b\n"+
        "p4twF3d5AgMBAAECggEBALWOnyvuanw7cbMHW/Vp2Wm82VnuRiSk8csPiwa/iQT6\n"+
        "9LQ1iWfqU9GJuU+uLblNs+aoEGXuBC4IbQNJdgSKySx16DsU7/ng/xUIDmcGt9hl\n"+
        "5sR7q1NJrh12n0udUqElW+Ku34j4TvfitBvQS1fDKZjLwkgzZ4ZWzOaiY37EGqjM\n"+
        "Rn29+1WqkNFtDOKCh07s5MoItrXF2qLsfd/A39qjaaru0HVuubw+51TOI76DeoyL\n"+
        "7I/9WEFg1yxmF5lzruQ+HlJkpz1bQosUfE1IhrOAprDbI/Q0PkM3I46O1W/y4IBg\n"+
        "3/xTeuEOPIbtjj+Tz/B/JfIePH7JAnDRZVju1fLTbgECgYEA/ZpIY9ZVmpXctO2F\n"+
        "KYrniiYmH3NG/0QpbIOmwYxVsRf+xMvQ2U7pYlizAr/kNxtAVvD7psTmn5lW+7Fy\n"+
        "YW5gCTGSwIGzWeZCeIT77g5qzIcV4RgPm4GOIxfbAGjZN60hUtQxLysRWs51HnjN\n"+
        "382QkJ7R1gLtMoufmGWzKw63wkECgYEA6F/JnqcfrxzWzKbsDKPh5Ka3e68YP+xW\n"+
        "/QD4pIPn+JbybkGDAdS0YKAekryIStr7jweSJAxWR0sbsGwSsMwz8c/BKzITsp8y\n"+
        "FKrUnHqCLPCcUx4Gkc2uBUmOPJdUpkZdIpPzeGgI60+3tyNtttFoS6FbQcBsQPUs\n"+
        "cYFaMdOhdzkCgYEAx8e+t0el8D2WvMFQYcWXWXFsHuKFT5q2t1GPEHIpH3WzsfyN\n"+
        "7mMH3/3YJ5Zbhj3llZgJ5vq+Y4Q8zdKgNLjq1tNCtHx4eZ23R5IblRGueKkCR2zY\n"+
        "/rw06NhmIhB27jFpSwL2LB5VcZ+rC/JG3MPn2sYCsAFmMqdjdn7Hgmf5wwECgYEA\n"+
        "ibGI1TwOqEMcfPREV1Ec39JyUxXz9pzjBtR2gXWwFTvMt8yWBpW1tLPsqeKRmFme\n"+
        "SqifzkCJN4GoDKhdgundteH3gtMPnP58PIblqTtmu55v40idd/Q6s+NcJ0c837I+\n"+
        "KeklkimDb3wzTo/tyTDY7kfUvWd6PhuJlFCj03al7PECgYEAj33wcZEhLbSd2dXw\n"+
        "OHuOkgCRm8P0QrgygOQtMnwSoYdJCE4HJIFArv6vjCJcRFMmHynZi3pg+pEocUKs\n"+
        "1b4if7gwHjLc30DxYYo5hRmJg5r5QFF4rZZsF0GBJjaEpBnY15LAgpyA42ZWHsyA\n"+
        "NWtkXSzj8JmEfszEaz6BklfEb2w=\n"+
        "-----END PRIVATE KEY-----";
        
    @Autowired 
    CertificateService service;

    @Test
    public void exportEmptyCertifiateToPkcs12AndFail() {
        Certificate cert = new Certificate();

        assertThatThrownBy(() -> service.buildPkcs12FromCert(cert)).isInstanceOf(RuntimeException.class);
    }

    @Test
    public void exportCertifiateToPkcs12() {
        Certificate cert = createCertFromClassPath();

        assertThat(service.buildPkcs12FromCert(cert)).isNotNull();
    }

    private Certificate createCertFromClassPath() {
        Certificate cert = new Certificate();

        cert.setCertificate(crt);
        cert.setKey(key);

        return cert;
    }
}