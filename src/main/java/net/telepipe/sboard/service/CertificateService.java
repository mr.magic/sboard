package net.telepipe.sboard.service;

import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.telepipe.sboard.model.Certificate;
import net.telepipe.sboard.model.Pkcs12;
import net.telepipe.sboard.repository.CertificateRepository;
import net.telepipe.sboard.utils.CryptoUtils;

/**
 * CertificateService
 */
@Service
public class CertificateService {
    @Autowired
    CertificateRepository repo;

    public List<Certificate> findByUserName(String userName) {
        return repo.findByUserName(userName);
    }

    public Pkcs12 buildPkcs12FromCert(Certificate cert) {
        try {
            String password = CryptoUtils.randomPassword(16);
            
            KeyStore ks = CryptoUtils.buildPkcs12Keystore(
                new X509Certificate[]{CryptoUtils.decodeCertificate(cert.getCertiticate())},
                CryptoUtils.decodePrivateKey(cert.getKey()),
                password
            );

            return Pkcs12.FromKeystore(ks, password);
        } catch (Exception e) {
            throw new RuntimeException("Failed to build P12", e);
        }
    }
}