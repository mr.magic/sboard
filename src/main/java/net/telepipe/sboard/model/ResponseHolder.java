package net.telepipe.sboard.model;

/**
 * ResponseHolder
 */
public class ResponseHolder {

    Object payload;

    String logLevel = "INFO";

    Long nextPoll = 1000L;

    public Object getPayload() {
        return payload;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public Long getNextPoll() {
        return nextPoll;
    }

    public ResponseHolder(Object payload) {
        this.payload = payload;
    }
}