package net.telepipe.sboard.model;

import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import net.telepipe.sboard.utils.CryptoUtils;

/**
 * Certificate
 */
@Entity
@Table(name = "CERTIFICATES")
public class Certificate {
    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "CERT", columnDefinition = "TEXT")
    private String certificate;

    @Column(name = "KEY", columnDefinition = "TEXT")
    private String key;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    public Certificate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCertiticate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFingerPrint() {
        X509Certificate x509 = CryptoUtils.decodeCertificate(this.certificate);

        try {
            return CryptoUtils.hashData(x509.getEncoded(), "SHA-1");
        } catch (CertificateEncodingException e) {
            throw new RuntimeException("Failed to build fingerprint", e);
        }
    }
}