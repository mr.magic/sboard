 package net.telepipe.sboard.model;
 
 /**
  * CertificateAction
  */
 public class CertificateAction {
    public static final String ACTION_ADD = "A";
    public static final String ACTION_DEL = "D";
 
    String action;

    String certificateHash;

    public String getAction() {
        return action;
    }

    public String getCertificateHash() {
        return certificateHash;
    }

    public CertificateAction(String action, String certificateHash) {
        this.action = action;
        this.certificateHash = certificateHash;
    }
 }