package net.telepipe.sboard.model;

import java.io.ByteArrayOutputStream;
import java.security.KeyStore;

/**
 * Pkcs12
 */
public class Pkcs12 {
    String password;

    byte[] bytes;

    public static Pkcs12 FromKeystore(KeyStore ks, String password) {
        if( !ks.getType().equals("PKCS12") ) {
            throw new RuntimeException("No PKCS12 Keystore passed");
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ks.store(baos, password.toCharArray());
    
            return new Pkcs12(baos.toByteArray(), password);
        } catch (Exception e) {
            throw new RuntimeException("Failed to export Keystore", e);
        }
    }

    public Pkcs12(byte[] bytes, String password) {
        this.password = password;
        this.bytes = bytes;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}