package net.telepipe.sboard.model;

import javax.xml.bind.DatatypeConverter;

/**
 * CertificateDownloadResponse
 */
public class CertificateDownloadResponse {
    private Pkcs12 p12;

    public String getP12Blob() {
        return DatatypeConverter.printBase64Binary(p12.bytes);
    }

    public String getPassword() {
        return DatatypeConverter.printBase64Binary(p12.password.getBytes());
    }

    public CertificateDownloadResponse(Pkcs12 p12) {
        this.p12 = p12;
    }
}