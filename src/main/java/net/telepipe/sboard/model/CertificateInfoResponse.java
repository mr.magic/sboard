package net.telepipe.sboard.model;

import java.util.List;

/**
 * CertificateAction
 */
public class CertificateInfoResponse {
    List<CertificateAction> certificateHashes;

    public List<CertificateAction> getCertificateHashes() {
        return certificateHashes;
    }

    public CertificateInfoResponse(List<CertificateAction> certificateActions) {
        this.certificateHashes = certificateActions;
    }
}