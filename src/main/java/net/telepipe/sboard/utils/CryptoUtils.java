package net.telepipe.sboard.utils;

import java.io.ByteArrayInputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

/**
 * CryptoUtils
 */
public class CryptoUtils {

    public static X509Certificate decodeCertificate(String pem) {
        try {
            CertificateFactory certFactory;

            certFactory = CertificateFactory.getInstance("X.509");

            X509Certificate cert = (X509Certificate) certFactory
                    .generateCertificate(new ByteArrayInputStream(pem.getBytes()));

            return cert;
        } catch (Exception e) {
            throw new RuntimeException("Failed to decode certifiate", e);
        }
    }

    public static PrivateKey decodePrivateKey(String pem) {
        try {
            Pattern parse = Pattern.compile("(?m)(?s)^---*BEGIN.*---*$(.*)^---*END.*---*$.*");
            String b64 = parse.matcher(pem).replaceFirst("$1");

            byte[] encoded = DatatypeConverter.parseBase64Binary(b64);

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey key = keyFactory.generatePrivate(keySpec);

            return key;
        } catch (Exception e) {
            throw new RuntimeException("Failed to decode private key", e);
        }
    }

    public static String hashData(byte[] data, String algorithm) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);

            md.update(data);

            String fp = DatatypeConverter.printHexBinary(md.digest());

            return fp;

        } catch (Exception e) {
            throw new RuntimeException("Failed to hash data", e);
        }
    }

    public static String randomPassword(int count) {
        final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        return builder.toString();
    }

    public static KeyStore buildPkcs12Keystore(X509Certificate[] chain, PrivateKey key, String password) {
        try {
            KeyStore p12 = KeyStore.getInstance("PKCS12");
            p12.load(null, null);
            p12.setKeyEntry("privateKey", key, password.toCharArray(), chain);

            return p12;
        } catch (Exception e) {
            throw new RuntimeException("Failed to create PKCS#12 Keystore" , e);
        }
    }
}