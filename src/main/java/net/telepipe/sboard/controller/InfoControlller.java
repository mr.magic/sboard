package net.telepipe.sboard.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.telepipe.sboard.model.CertificateAction;
import net.telepipe.sboard.model.CertificateInfoResponse;
import net.telepipe.sboard.model.ResponseHolder;
import net.telepipe.sboard.service.CertificateService;

/**
 * InfoControlller
 */
@RestController
@RequestMapping("info/{user}")
public class InfoControlller {
    @Autowired
    CertificateService service;

    @GetMapping(path = "/certificates", produces = "application/json")
    public ResponseHolder getFingerPrints(@PathVariable("user") String user) {
        //Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<CertificateAction> actions = service.findByUserName(user).stream()
            .map(certificate -> new CertificateAction(CertificateAction.ACTION_ADD, certificate.getFingerPrint()))
            .collect(Collectors.toList());

        return new ResponseHolder(new CertificateInfoResponse(actions));
    }

    
}