package net.telepipe.sboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.telepipe.sboard.model.CertificateDownloadResponse;
import net.telepipe.sboard.model.Pkcs12;
import net.telepipe.sboard.model.ResponseHolder;
import net.telepipe.sboard.service.CertificateService;

/**
 * DownloadController
 */
@RestController
@RequestMapping("/download/{user}")
public class DownloadController {
    @Autowired
    CertificateService service;
    
    @GetMapping(path = "/certificates/{fingerPrint}", produces = "application/json")
    public ResponseHolder getCertificate(@PathVariable("user") String user, @PathVariable("fingerPrint") String fingerPrint) {
        Pkcs12 p12 = this.service.findByUserName(user).stream()
            .filter(c -> c.getFingerPrint().equalsIgnoreCase(fingerPrint))
            .findFirst()
            .map(cert -> this.service.buildPkcs12FromCert(cert))
            .get();

        return new ResponseHolder(new CertificateDownloadResponse(p12));
    }
}